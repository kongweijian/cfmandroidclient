package com.example.bluetooth.le.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.LoginActivity;
import com.example.bluetooth.le.MainActivity;
import com.example.bluetooth.le.Message;
import com.example.bluetooth.le.RegisterActivity;
import com.example.bluetooth.le.comm.MacAddress;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;

import java.util.HashMap;
import java.util.Map;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;


public class HomeFragment extends Fragment implements Message{

    private ImageView stateImg;
    private String message;
    private  View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container,false);
        stateImg = (ImageView) view.findViewById(R.id.iv_state);
      //  receiver = new BroadcastMain();

        getDeviceState();

        return view;
    }

    public void getDeviceState(){

    }

    @Override
    public void onHiddenChanged(boolean hidd) {

    }

    @Override
    public void getMessageSuccess(String msg) {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

}
