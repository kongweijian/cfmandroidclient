package com.example.bluetooth.le;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView.OnEditorActionListener;

import cfm.com.cfmclient.R;


public class MyEditText extends LinearLayout implements TextWatcher {

    private EditText editText;
    private ImageView imageView;

    public MyEditText(Context context) {
        super(context);
        init(context, null, 0);
    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public MyEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public void setHint(int ResId) {
        editText.setHint(ResId);
    }

    public void setHint(String res) {
        editText.setHint(res);
    }

    public void setText(String text) {
    	if(TextUtils.isEmpty(text)) {
    		text = "";
    	}
        editText.setText(text);
        editText.setSelection(text.length());
    }

    public void setSelection(int index) {
        editText.setSelection(index);
    }

    public void setInputType(int type) {
        editText.setInputType(type);
    }

    public EditText getEditText() {
        return editText;
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        setPadding(0, 0, 0, 0);
        setGravity(Gravity.CENTER);

        editText = new EditText(context, attrs);
        editText.setBackgroundColor(0);
        editText.addTextChangedListener(this);

        LayoutParams editLayoutParams = new LayoutParams(0, android.view.ViewGroup.LayoutParams.MATCH_PARENT, 1);
        editLayoutParams.setMargins(0, 0, 0, 0);
        addView(editText, editLayoutParams);

        imageView = new ImageView(context);
        imageView.setVisibility(View.GONE);
        imageView.setImageResource(R.drawable.ic_del);
        imageView.setPadding(getResources().getDimensionPixelSize(R.dimen.edit_text_padding), 0,
                getResources().getDimensionPixelSize(R.dimen.edit_text_padding), 0);
        imageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });

        LayoutParams imageLayoutParams = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        addView(imageView, imageLayoutParams);
    }

    public void setText(CharSequence text) {
        editText.setText(text);
    }

    public void setText(int resid) {
        editText.setText(resid);
    }

    public Editable getText() {
        return editText.getText();
    }
    
    public void setEditTextGravity(int gravity) {
    	editText.setGravity(gravity);
    }
    
    @Override
    public void setOnClickListener(OnClickListener listener) {
        editText.setOnClickListener(listener);
    }

    public void setImeOptions(int imeOptions) {
        editText.setImeOptions(imeOptions);
    }

    public void setImeActionLabel(CharSequence label, int actionId) {
        editText.setImeActionLabel(label, actionId);
    }

    public void setOnEditorActionListener(OnEditorActionListener listener) {
        editText.setOnEditorActionListener(listener);
    }

    public void addTextChangedListener(TextWatcher listener) {
        editText.addTextChangedListener(listener);
    }

    public void removeTextChangedListener(TextWatcher listener) {
        editText.removeTextChangedListener(listener);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        editText.setEnabled(enabled);
        if (!enabled) {
            imageView.setVisibility(View.GONE);
        }
    }

    public void setTextSize(float size) {
        editText.setTextSize(size);
    }
    
    public void setTextSize(int unit, float size) {
        editText.setTextSize(unit, size);
    }

    public void setTextColor(int color) {
        editText.setTextColor(color);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (isEnabled() && s.length() > 0) {
            imageView.setVisibility(View.VISIBLE);
        } else {
            imageView.setVisibility(View.GONE);
        }
    }

}
