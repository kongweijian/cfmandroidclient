package com.example.bluetooth.le.comm;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.bluetooth.le.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;


/**
 * Created by KWJ on 2017/6/23.
 */
public class DBManager {
    private DatabaseHelper helper;
    private SQLiteDatabase db;

    public DBManager(Context context)
    {
      ///  Log.d(AppConstants.LOG_TAG, "DBManager --> Constructor");
        helper = new DatabaseHelper(context);
        // 因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0,
        // mFactory);
        // 所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里
        db = helper.getWritableDatabase();
    }

    /**
     * add persons
     *
     * @param macAddress
     */
    public void add(String macAddress)
    {
       // Log.d(AppConstants.LOG_TAG, "DBManager --> add");
        // 采用事务处理，确保数据完整性
        db.beginTransaction(); // 开始事务
        try
        {
//            for (Date person : persons)
//            {
                db.execSQL("INSERT INTO " + DatabaseHelper.TABLE_NAME
                        + " VALUES(null, ?)", new Object[] { macAddress});
                // 带两个参数的execSQL()方法，采用占位符参数？，把参数值放在后面，顺序对应
                // 一个参数的execSQL()方法中，用户输入特殊字符时需要转义
                // 使用占位符有效区分了这种情况
//            }
            db.setTransactionSuccessful(); // 设置事务成功完成
        }
        finally
        {
            db.endTransaction(); // 结束事务
        }
    }


//    public void updateAge(Person person)
//    {
//       // Log.d(AppConstants.LOG_TAG, "DBManager --> updateAge");
//        ContentValues cv = new ContentValues();
//        cv.put("age", person.age);
//        db.update(DatabaseHelper.TABLE_NAME, cv, "name = ?",
//                new String[] { person.name });
//    }

    /**
     * delete old person
     *
     * @param person
     */


    /**
     * query all persons, return list
     *
     * @return List<Person>
     */
    public List<MacModel> query()
    {
     //   Log.d(AppConstants.LOG_TAG, "DBManager --> query");
        ArrayList<MacModel> persons = new ArrayList<MacModel>();
        Cursor c = queryTheCursor();
        while (c.moveToNext())
        {
            MacModel person = new MacModel();
            person._id = c.getInt(c.getColumnIndex("_id"));
            person.macAddress = c.getString(c.getColumnIndex("mac"));
            persons.add(person);
        }
        c.close();
        return persons;
    }

    /**
     * query all persons, return cursor
     *
     * @return Cursor
     */
    public Cursor queryTheCursor()
    {
       // Log.d(AppConstants.LOG_TAG, "DBManager --> queryTheCursor");
        Cursor c = db.rawQuery("SELECT * FROM " + DatabaseHelper.TABLE_NAME,
                null);
        return c;
    }

    /**
     * close database
     */
    public void closeDB()
    {
       // Log.d(AppConstants.LOG_TAG, "DBManager --> closeDB");
        // 释放数据库资源
        db.close();
    }

}