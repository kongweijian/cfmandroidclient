package com.example.bluetooth.le;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.MacAddress;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

public class UpdatePwdActivity extends TitleActivity {

    private EditText pwdEdt,newPwdEdt,inputPwdEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("修改密码");
        showBackwardView(0, true);
        setContentView(R.layout.activity_update_pwd);

        pwdEdt = (EditText)findViewById(R.id.pwd_edt);
        newPwdEdt = (EditText)findViewById(R.id.newpwd_edt);
        inputPwdEdt = (EditText)findViewById(R.id.inputpwd_edt);

        Button sureBtn = (Button) findViewById(R.id.sure_btn);
        sureBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                //checkPwd();
                if(checkPwd()){
                    if(checkEdit()){
                        modifyPwd();
                    }
                }else{
                    Toast.makeText(UpdatePwdActivity.this, "两次输入不一致", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private Boolean checkPwd(){

        if(newPwdEdt.getText().toString().equals(inputPwdEdt.getText().toString())){
            return true;
        }else{
            return false;
        }
    }

    private boolean checkEdit(){

        if(pwdEdt.getText().toString().trim().equals("")){
            Toast.makeText(UpdatePwdActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
        }else if(newPwdEdt.getText().toString().trim().equals("")){
            Toast.makeText(UpdatePwdActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
        }else if (inputPwdEdt.getText().toString().trim().equals("")){
            Toast.makeText(UpdatePwdActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
        }else{
            return true;
        }
        return false;
    }

    private void modifyPwd(){

        String httpUrl = Url.baseUrl+Url.modifyPwd;
        final Map<String, String> map = new HashMap<String, String>();
        map.put("userName", UserName.getInstance().getUserName());
        map.put("passWord", newPwdEdt.getText().toString().trim());

        StringRequest post = new StringRequest(Request.Method.POST, httpUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                System.out.println( "volleyPostStringMonth请求成功：" + s);
                String state = null;
                String macAddress = null;
                try {
                    JSONObject object = new JSONObject(s);
                    state = object.getString("state");
                    if(state.equals("success")){
                        Toast.makeText(UpdatePwdActivity.this,"修改成功！", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(UpdatePwdActivity.this, state, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(UpdatePwdActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //创建一个集合，放的是keyvalue的key是参数名与value是参数值
                return map;
            }
        };
        MyApp.requestQueue.add(post);
    }
}
