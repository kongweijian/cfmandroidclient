package com.example.bluetooth.le;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import cfm.com.cfmclient.R;

public class AboutActivity extends TitleActivity implements OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("关于");
		showForwardView(false);
		showBackwardView(0, true);
		setContentView(R.layout.activity_about);

	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
	    case R.id.button_backward:
            finish();
            break;
	    default:
			break;
		}
		
	}

}
