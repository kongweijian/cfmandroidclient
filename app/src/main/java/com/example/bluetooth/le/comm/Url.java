package com.example.bluetooth.le.comm;

public class Url {

    public static String baseUrl = "http://106.14.190.177:8082/cfm1";

    public static String register = "/account/register";

    public static String login = "/account/login";

    public static String bindHistory = "/device/bindList";

    public static String bindedHistory = "/device/bindedList";

    public static String warnHistory = "/warning/history";

    public static String modifyPwd = "/account/modifyPwd";

    public static String infors = "/warning/infors";

    public static String bind = "/device/bind";

    public static String macs = "/device/macs";

    public static String childName = "/device/child";

    public static String bindChild = "/device/bindChild";

    public static String deleteBindedChild = "/device/deleteBindChild";

    public static String setUrl(String path){
        return baseUrl+path;
    }

}
