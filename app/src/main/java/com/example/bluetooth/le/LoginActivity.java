package com.example.bluetooth.le;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.DBManager;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;
import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;


import static com.android.volley.Request.Method;

public class LoginActivity extends Activity implements OnClickListener{

	private Button btnRegister,btnLogin,btnPwd;
	private EditText etUserName,etPassword;
	private DBManager dbManager;
	private static final float BEEP_VOLUME = 0.10f;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		etUserName = (EditText) findViewById(R.id.et_username);
		etPassword = (EditText) findViewById(R.id.et_password);
		btnRegister = (Button)findViewById(R.id.btn_sign_up);
		btnLogin = (Button)findViewById(R.id.btn_login);
		btnPwd = (Button)findViewById(R.id.btn_forgot_password);
		btnRegister.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		btnPwd.setOnClickListener(this);
//		//VibratorUtil.PlaySound(LoginActivity.this);
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
       switch (view.getId()) {
		
	    case R.id.btn_sign_up:
	    	Intent intent1 = new Intent(LoginActivity.this,  RegisterActivity.class);
	    	startActivity(intent1);
            break;
	    case R.id.btn_login:
			if(checkEdit()){
				login();
			}
            break;
		 case R.id.btn_forgot_password:
			   //	VibratorUtil.PlaySound(LoginActivity.this);
			Intent intent3 = new Intent(LoginActivity.this, FindPwdActivity.class);
			startActivity(intent3);
			break;
	    default:
			break;
		}
	}

	private boolean checkEdit(){

		if(etUserName.getText().toString().trim().equals("")){
			Toast.makeText(LoginActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();
		}else if(etPassword.getText().toString().trim().equals("")){
			Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
		}else{
			return true;
		}
		return false;
	}

	private void login(){

		String httpUrl = Url.baseUrl+Url.login;
		final Map<String, String> map = new HashMap<String, String>();
		map.put("userName", etUserName.getText().toString().trim());
		map.put("passWord", etPassword.getText().toString().trim());
        StringRequest post = new StringRequest(Method.POST, httpUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                System.out.println( "volleyPostStringMonth请求成功：" + s);

                try {
                    JSONObject object = new JSONObject(s);
					String state = object.getString("state");
                    if(state.equals("success")){
                    //    MacAddress.setInstance(new MacAddress(macAddress));
						UserName.setInstance(new UserName(etUserName.getText().toString().trim()));
                        Intent intent2 = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent2);

						EMClient.getInstance().login(map.get("userName"),map.get("userName").charAt(0)+"1",new EMCallBack() {//回调
							@Override
							public void onSuccess() {
								EMClient.getInstance().groupManager().loadAllGroups();
								EMClient.getInstance().chatManager().loadAllConversations();
								Log.d("main", "登录聊天服务器成功！");
							}

							@Override
							public void onProgress(int progress, String status) {

							}

							@Override
							public void onError(int code, String message) {
								Log.d("main", "登录聊天服务器失败！");
							}
						});
                    }else{
                        Toast.makeText(LoginActivity.this, state, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(LoginActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //创建一个集合，放的是keyvalue的key是参数名与value是参数值
                return map;
            }
        };
		MyApp.requestQueue.add(post);
	}

	private void  registerJpush(){

		String httpUrl = Url.baseUrl+Url.macs;
		StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?userName="+
				UserName.getInstance().getUserName(), new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {
				try {
					JSONObject object = new JSONObject(s);
					JSONArray macs = object.getJSONArray("macs");
					if(macs.length()>0) {
						Set<String> set = new HashSet<String>();
						for (int i = 0; i < macs.length(); i++) {

							JSONObject mac =  macs.getJSONObject(i);
							set.add(mac.getString("macAddress").replace(":",""));
						}
					}else{
						Toast.makeText(LoginActivity.this, "尚未绑定设备！", Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {

				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Toast.makeText(LoginActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
			}
		});
		MyApp.requestQueue.add(get);
	}

	@Override
	protected void onResume() {
		super.onResume();
		System.out.println("================");
		EMClient.getInstance().logout(true);
	}
}
