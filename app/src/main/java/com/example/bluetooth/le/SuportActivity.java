package com.example.bluetooth.le;

import android.app.Activity;
import android.os.Bundle;

import cfm.com.cfmclient.R;

public class SuportActivity extends TitleActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("技术支持");
        showBackwardView(0, true);
        setContentView(R.layout.activity_suport);
    }
}
