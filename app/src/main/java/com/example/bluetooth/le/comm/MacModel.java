package com.example.bluetooth.le.comm;

/**
 * Created by KWJ on 2017/6/23.
 */
public class MacModel {

    public int _id;
    public String macAddress;

    public MacModel() {
    }

    public MacModel(String macAddress)
    {
        this.macAddress = macAddress;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}
