package com.example.bluetooth.le.comm;

public class MacAddress {

    private String macAddress;

	private volatile static MacAddress instance;

	public MacAddress(String macAddress){
		this.macAddress=macAddress;
	}

	public MacAddress(){

	}

	public static MacAddress getInstance() {
		if (instance == null) {
			instance = new MacAddress();
		}
		return instance;
	}

	public static void setInstance(MacAddress instance) {
		MacAddress.instance = instance;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
}
