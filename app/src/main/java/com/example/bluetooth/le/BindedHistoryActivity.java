package com.example.bluetooth.le;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

public class BindedHistoryActivity extends TitleActivity implements View.OnClickListener{

    private ListView listView;
    private SimpleAdapter simp_adapter;
    private List<Map<String,Object>> dataList;
    private int index=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("已关联病人");
        showBackwardView(0, true);
        setContentView(R.layout.activity_binded_history);

        listView = (ListView) findViewById(R.id.listView);
        listView.addFooterView(new ViewStub(MyApp.getContext()));
        dataList = new ArrayList<Map<String,Object>>();

        getBindedHistoryData();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                deleteDialog(position);
            }
        });
    }


    private void deleteDialog(final int position){

        AlertDialog.Builder builder = new AlertDialog.Builder(
                BindedHistoryActivity.this,AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("删除");
        builder.setMessage("是否确定删除已关联的病人？");
        builder.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteBindedChild(position);
                        dataList.remove(position);
                        simp_adapter.notifyDataSetChanged();
                    }
                });
        builder.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //setTitle("点击了对话框上的Button3");
                    }
                });
        builder.show();
    }

    private void getBindedHistoryData(){

        String httpUrl =Url.setUrl(Url.bindedHistory);
        StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?userName="+
                UserName.getInstance().getUserName(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    JSONArray bindList = object.getJSONArray("bindList");
                    dataList.clear();
                    if(bindList.length()>0) {
                        for (int i = 0; i < bindList.length(); i++) {
                            Map<String, Object> map = new HashMap<String, Object>();
                            JSONObject obj =  bindList.getJSONObject(i);
                            map.put("img", R.drawable.baby);
                            map.put("id", obj.getString("id"));
                            map.put("tvName", obj.getString("childName"));
                            map.put("tvTime", "关联时间："+obj.getString("createTime"));
                            dataList.add(map);
                        }

                        simp_adapter = new SimpleAdapter(MyApp.getContext(), dataList, R.layout.binded_list_item,
                                new String[]{"img","tvName","tvTime"}, new int[]{R.id.ivImage,R.id.tvName,R.id.tvTime});
                        listView.setAdapter(simp_adapter);
                    }else{
                        Toast.makeText(MyApp.getContext(), "无记录", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MyApp.getContext(), "网络错误", Toast.LENGTH_SHORT).show();
            }
        });
        MyApp.requestQueue.add(get);
    }

    private void deleteBindedChild(int index){

        Map<String, Object> map = dataList.get(index);
        String httpUrl = Url.setUrl(Url.deleteBindedChild);
        System.out.println("aa======================"+httpUrl+"?id="+
                map.get("id"));
        StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?id="+
                map.get("id"), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Toast.makeText(MyApp.getContext(), "已解除关联！", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MyApp.getContext(), "网络错误", Toast.LENGTH_SHORT).show();
            }
        });
        MyApp.requestQueue.add(get);
    }
}
