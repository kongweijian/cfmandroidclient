package com.example.bluetooth.le.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.bluetooth.le.AboutActivity;
import com.example.bluetooth.le.BindActivity;
import com.example.bluetooth.le.BindHistoryActivity;
import com.example.bluetooth.le.BindedHistoryActivity;
import com.example.bluetooth.le.MainActivity;
import com.example.bluetooth.le.SuportActivity;
import com.example.bluetooth.le.UpdatePwdActivity;
import com.hyphenate.chat.EMClient;

import cfm.com.cfmclient.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetFragment extends Fragment implements View.OnClickListener {

    private RelativeLayout pwd_rl,update_rl,about_rl,lx_rl,exit_rl,wgl_rl,ygl_rl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_set2, container,false);
        pwd_rl = (RelativeLayout) view.findViewById(R.id.pwd_rl);
        update_rl = (RelativeLayout) view.findViewById(R.id.update_rl);
        exit_rl = (RelativeLayout) view.findViewById(R.id.rl_exit);
        about_rl = (RelativeLayout) view.findViewById(R.id.about_rl);
        lx_rl = (RelativeLayout) view.findViewById(R.id.lx_rl);
        wgl_rl = (RelativeLayout) view.findViewById(R.id.wgl_rl);
        ygl_rl = (RelativeLayout) view.findViewById(R.id.ygl_rl);

        pwd_rl.setOnClickListener(this);
        update_rl.setOnClickListener(this);
        exit_rl.setOnClickListener(this);
        about_rl.setOnClickListener(this);
        lx_rl.setOnClickListener(this);
        wgl_rl.setOnClickListener(this);
        ygl_rl.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.pwd_rl:
                Intent intent1=new Intent(getActivity(), UpdatePwdActivity.class);
                startActivity(intent1);
                break;
            case R.id.update_rl:
                Intent intent2=new Intent(getActivity(), BindActivity.class);
                startActivity(intent2);
                break;
            case R.id.about_rl:
                Intent intent3=new Intent(getActivity(), AboutActivity.class);
                startActivity(intent3);
                break;
            case R.id.lx_rl:
                Intent intent4=new Intent(getActivity(), SuportActivity.class);
                startActivity(intent4);
                break;
            case R.id.wgl_rl:
                Intent intent5=new Intent(getActivity(), BindHistoryActivity.class);
                startActivity(intent5);
                break;
            case R.id.ygl_rl:
                Intent intent6=new Intent(getActivity(), BindedHistoryActivity.class);
                startActivity(intent6);
                break;
            case R.id.rl_exit:
                getActivity().finish();
                break;
        }
    }

    private void update(){

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("检查更新");
        builder.setMessage("已是最新版本");
        builder.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                });
        builder.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //setTitle("点击了对话框上的Button3");
                    }
                });
        builder.show();
    }

    private void exitAPP(){

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle("退出应用");
        builder.setMessage("是否退出应用？");
        builder.setPositiveButton("确定",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MainActivity.homeUI.logout();
                        //android.os.Process.killProcess(android.os.Process.myPid());
                        //ActivityManager.getInstance().exit();
                    }
                });
        builder.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //setTitle("点击了对话框上的Button3");
                    }
                });
        builder.show();
    }
}
