package com.example.bluetooth.le;



public class Position {
	
	private int x,y,z;

	private volatile static Position instance;
	
	
	public Position(){
		
	}
	
	public static Position getInstance() {
		if (instance == null) {
			instance = new Position();
		}
		return instance;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public static void setInstance(Position instance) {
		Position.instance = instance;
	}
	
	
}
