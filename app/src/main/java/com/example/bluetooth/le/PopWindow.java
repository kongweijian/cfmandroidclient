package com.example.bluetooth.le;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;

import cfm.com.cfmclient.R;

/**
 * <p>Title:PopWindow</p>
 * <p>Description: 自定义PopupWindow</p>
 * @author syz
 * @date 2016-3-14
 */
public class PopWindow extends PopupWindow{
	private View conentView;
	public PopWindow(final Activity context){
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		conentView = inflater.inflate(R.layout.popup_window, null);
		int h = context.getWindowManager().getDefaultDisplay().getHeight();
		int w = context.getWindowManager().getDefaultDisplay().getWidth();
		// 设置SelectPicPopupWindow的View
		this.setContentView(conentView);
		// 设置SelectPicPopupWindow弹出窗体的宽
		this.setWidth(w / 2 + 40);
		// 设置SelectPicPopupWindow弹出窗体的高
		this.setHeight(LayoutParams.WRAP_CONTENT);
		// 设置SelectPicPopupWindow弹出窗体可点击
		this.setFocusable(true);
		this.setOutsideTouchable(true);
		// 刷新状态
		this.update();
		// 实例化一个ColorDrawable颜色为半透明
		ColorDrawable dw = new ColorDrawable(0000000000);
		// 点back键和其他地方使其消失,设置了这个才能触发OnDismisslistener ，设置其他控件变化等操作
		this.setBackgroundDrawable(dw);
		// mPopupWindow.setAnimationStyle(android.R.style.Animation_Dialog);
		// 设置SelectPicPopupWindow弹出窗体动画效果
		this.setAnimationStyle(R.style.AnimationPreview);
		
		conentView.findViewById(R.id.about).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//do something you need here
				Intent intent = new Intent(context, AboutActivity.class);
				context.startActivity(intent);
				PopWindow.this.dismiss();
			}
		});
		conentView.findViewById(R.id.ability_logout).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// do something before signing out
				//context.finish();
				exitAPP(context);
				//android.os.Process.killProcess(android.os.Process.myPid());
				PopWindow.this.dismiss();
			}
		});
		conentView.findViewById(R.id.settings).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// do something you need here 
				 Intent intent = new Intent(context, SetActivity.class);
				 context.startActivity(intent);
				PopWindow.this.dismiss();
			}
		});
	}

	private void exitAPP(Activity context){

		AlertDialog.Builder builder = new AlertDialog.Builder(
				context,AlertDialog.THEME_HOLO_LIGHT);
		builder.setTitle("退出应用");
		builder.setMessage("是否退出应用？");
		builder.setPositiveButton("确定",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						DeviceScanActivity.homeUI.logout();
						//android.os.Process.killProcess(android.os.Process.myPid());
						//ActivityManager.getInstance().exit();
					}
				});
		builder.setNegativeButton("取消",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						//setTitle("点击了对话框上的Button3");
					}
				});
		builder.show();
	}


	/**
	 * 显示popupWindow
	 * 
	 * @param parent
	 */
	public void showPopupWindow(View parent) {
		if (!this.isShowing()) {
			// 以下拉方式显示popupwindow
			this.showAsDropDown(parent, parent.getLayoutParams().width / 3, 11);
		} else {
			this.dismiss();
		}
	}
}
