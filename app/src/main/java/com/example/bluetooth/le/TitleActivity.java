/*******************************************************************************
 *
 * Copyright (c) Weaver Info Tech Co. Ltd
 *
 * TitleActivity
 *
 * app.client.TitleActivity.java
 * TODO: File description or class description.
 *
 * @author: gao_chun
 * @since:  2014-9-03
 * @version: 1.0.0
 *
 * @changeLogs:
 *     1.0.0: First created this class.
 *
 ******************************************************************************/
package com.example.bluetooth.le;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import cfm.com.cfmclient.R;

/**
 * @author gao_chun
 * 鑷畾涔夋爣棰樻爮
 */
public class TitleActivity extends FragmentActivity implements OnClickListener{

    //private RelativeLayout mLayoutTitleBar;
    public TextView mTitleTextView;
    public Button mBackwardbButton;
    public Button mForwardButton;
    public FrameLayout mContentLayout;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();   //鍔犺浇 activity_title 甯冨眬 锛屽苟鑾峰彇鏍囬鍙婁袱渚ф寜閽?
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, ev)) {
                hideKeyboard(v.getWindowToken());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    private boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                // 点击EditText的事件，忽略它。
                return false;
            } else {
                return true;
            }
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditText上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 获取InputMethodManager，隐藏软键盘
     * @param token
     */
    private void hideKeyboard(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    
    private void setupViews() {
        super.setContentView(R.layout.activity_title);
        mTitleTextView = (TextView) findViewById(R.id.text_title);
        mContentLayout = (FrameLayout) findViewById(R.id.layout_content);
        mBackwardbButton = (Button) findViewById(R.id.button_backward);
        mForwardButton = (Button) findViewById(R.id.button_forward);
        
       
    }

    /**
     * 鏄惁鏄剧ず杩斿洖鎸夐挳
     * @param backwardResid  鏂囧瓧
     * @param show  true鍒欐樉绀?
     */
    protected void showBackwardView(int backwardResid, boolean show) {
        if (mBackwardbButton != null) {
            if (show) {
                //mBackwardbButton.setText(backwardResid);
                mBackwardbButton.setVisibility(View.VISIBLE);
            } else {
                mBackwardbButton.setVisibility(View.INVISIBLE);
            }
        } // else ignored
    }

    /**
     * 鎻愪緵鏄惁鏄剧ず鎻愪氦鎸夐挳
     * @param
     * @param show  true鍒欐樉绀?
     */
    protected void showForwardView(boolean show) {
        if (mForwardButton != null) {
            if (show) {
                mForwardButton.setVisibility(View.VISIBLE);
                //mForwardButton.setText(forwardResId);
            } else {
                mForwardButton.setVisibility(View.INVISIBLE);
            }
        } // else ignored
    }

    /**
     * 杩斿洖鎸夐挳鐐瑰嚮鍚庤Е鍙?
     * @param backwardView
     */
    protected void onBackward(View backwardView) {
    	
        finish();
    }

    /**
     * 鎻愪氦鎸夐挳鐐瑰嚮鍚庤Е鍙?
     * @param forwardView
     */
    protected void onForward(View forwardView) {
    	finish();
    	 //android.os.Process.killProcess(android.os.Process.myPid());
    }


    //璁剧疆鏍囬鍐呭
    @Override
    public void setTitle(int titleId) {
        mTitleTextView.setText(titleId);
    }

    //璁剧疆鏍囬鍐呭
    @Override
    public void setTitle(CharSequence title) {
        mTitleTextView.setText(title);
    }

    //璁剧疆鏍囬鏂囧瓧棰滆壊
    @Override
    public void setTitleColor(int textColor) {
        mTitleTextView.setTextColor(textColor);
    }


    //鍙栧嚭FrameLayout骞惰皟鐢ㄧ埗绫籸emoveAllViews()鏂规硶
    @Override
    public void setContentView(int layoutResID) {
        mContentLayout.removeAllViews();
        View.inflate(this, layoutResID, mContentLayout);
        onContentChanged();
    }

    @Override
    public void setContentView(View view) {
        mContentLayout.removeAllViews();
        mContentLayout.addView(view);
        onContentChanged();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#setContentView(android.view.View, android.view.ViewGroup.LayoutParams)
     */
    @Override
    public void setContentView(View view, LayoutParams params) {
        mContentLayout.removeAllViews();
        mContentLayout.addView(view, params);
        onContentChanged();
    }


    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     * 鎸夐挳鐐瑰嚮璋冪敤鐨勬柟娉?
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button_backward:
                onBackward(v);
                break;

            case R.id.button_forward:
               // onForward(v);
                break;

            default:
                break;
        }
    }
}
