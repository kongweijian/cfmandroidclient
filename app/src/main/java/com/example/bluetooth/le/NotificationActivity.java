package com.example.bluetooth.le;

import android.app.Activity;
import android.app.NotificationManager;
import android.os.Bundle;

import cfm.com.cfmclient.R;

public class NotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //cancel()方法中的参数是我们在启动通知，调用manager.notify(1, notification)方法时，传的id
        manager.cancel(1);
    }
}
