package com.example.bluetooth.le;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.ForegroundCallbacks;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;
import com.example.bluetooth.le.fragment.HistoryFragment;
import com.example.bluetooth.le.fragment.HomeFragment;
import com.example.bluetooth.le.fragment.SetFragment;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.hyphenate.chat.EMMessageBody;
import com.hyphenate.chat.EMTextMessageBody;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

import static com.google.zxing.qrcode.decoder.ErrorCorrectionLevel.L;


public class MainActivity extends TitleActivity implements Message,EMMessageListener {


	private HomeFragment homeFragment;
	private HistoryFragment historyFragment;
	private SetFragment setFragment;
	private ImageView stateImg;
	private int currentId = R.id.tv_main;// 当前选中id,默认是主页
	MediaPlayer mediaPlayer01;
	private TextView tvMain, tvMessage, tvPerson;//底部四个TextView
	public static MainActivity homeUI;
	// 消息监听器
	private static EMMessageListener mMessageListener;
	private SpeechSynthesizer mTts;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getActionBar().hide();
		setContentView(R.layout.activity_main);
		stateImg = (ImageView)findViewById(R.id.iv_state);
		tvMain = (TextView) findViewById(R.id.tv_main);
		//tvMessage = (TextView) findViewById(R.id.tv_message);
		tvPerson = (TextView) findViewById(R.id.tv_person);
		tvMain.setSelected(true);//首页默认选中


		historyFragment = new HistoryFragment();
		mTitleTextView.setText(R.string.main);
		getSupportFragmentManager().beginTransaction().add(R.id.main_container, historyFragment).commit();

		tvMain.setOnClickListener(tabClickListener);
		tvPerson.setOnClickListener(tabClickListener);
		mMessageListener=this;
        mTts=SpeechSynthesizer.createSynthesizer(MyApp.getContext(),null);

		final IntentFilter filter = new IntentFilter();
		// 屏幕灭屏广播
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		// 屏幕亮屏广播
		filter.addAction(Intent.ACTION_SCREEN_ON);
		// 屏幕解锁广播
		filter.addAction(Intent.ACTION_USER_PRESENT);
		filter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
		BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(final Context context, final Intent intent) {
				//Log.d(TAG, "onReceive");
				String action = intent.getAction();
				System.out.println("正常");
				EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
			}
		};
		registerReceiver(mBatInfoReceiver, filter);

		ForegroundCallbacks.init(new MyApp());
		ForegroundCallbacks.get().addListener(new ForegroundCallbacks.Listener() {
			@Override
			public void onBecameForeground() {
				//L.d("当前程序切换到前台");

			}

			@Override
			public void onBecameBackground() {
				System.out.println("dgfdfhdfhdfdfdfd---------------------------------------");
				EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
			}
		});

	}

	private View.OnClickListener tabClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (v.getId() != currentId) {//如果当前选中跟上次选中的一样,不需要处理
				changeSelect(v.getId());//改变图标跟文字颜色的选中
				changeFragment(v.getId());//fragment的切换
				currentId = v.getId();//设置选中id
			}
		}
	};

	/**
	 * 改变fragment的显示
	 *
	 * @param resId
	 */
	private void changeFragment(int resId) {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();//开启一个Fragment事务

		hideFragments(transaction);//隐藏所有fragment
		if(resId==R.id.tv_main){//主页
			mTitleTextView.setText(R.string.message);
			if(historyFragment==null){
				historyFragment = new HistoryFragment();
				transaction.add(R.id.main_container,historyFragment);
			}else {
				transaction.show(historyFragment);
			}
		}else if(resId==000){//消息中心
			mTitleTextView.setText(R.string.message);
			if(historyFragment==null){
				historyFragment = new HistoryFragment();
				transaction.add(R.id.main_container,historyFragment);
			}else {
				transaction.show(historyFragment);
			}
		}else if(resId==R.id.tv_person){//我
			mTitleTextView.setText(R.string.person);
			if(setFragment==null){
				setFragment = new SetFragment();
				transaction.add(R.id.main_container,setFragment);
			}else {
				transaction.show(setFragment);
			}
		}
		transaction.commit();//一定要记得提交事务
	}

	/**
	 * 显示之前隐藏所有fragment
	 * @param transaction
	 */
	private void hideFragments(FragmentTransaction transaction){

		if (historyFragment != null)
			transaction.hide(historyFragment);
		if (setFragment != null)
			transaction.hide(setFragment);
	}

	/**
	 * 改变TextView选中颜色
	 * @param resId
	 */
	private void changeSelect(int resId) {
		tvMain.setSelected(false);
		//tvMessage.setSelected(false);
		tvPerson.setSelected(false);

		switch (resId) {
			case R.id.tv_main:
				tvMain.setSelected(true);
				break;
			case 000:
			//	tvMessage.setSelected(true);
				break;
			case R.id.tv_person:
				tvPerson.setSelected(true);
				break;
		}
	}

	private void  registerJpush(){

		String httpUrl = Url.baseUrl+Url.macs;
		StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?userName="+
				UserName.getInstance().getUserName(), new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {
				try {
					JSONObject object = new JSONObject(s);
					JSONArray macs = object.getJSONArray("macs");
					if(macs.length()>0) {
						Set<String> set = new HashSet<String>();
						for (int i = 0; i < macs.length(); i++) {

							JSONObject mac =  macs.getJSONObject(i);
							set.add(mac.getString("macAddress").replace(":",""));
						}
//						JPushInterface.setDebugMode(true);//正式版的时候设置false，关闭调试
//						JPushInterface.init(getApplicationContext());
//						JPushInterface.setTags(getApplicationContext(), set, null);//设置标签
					}else{
						Toast.makeText(MainActivity.this, "尚未绑定设备！", Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {

				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Toast.makeText(MainActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
			}
		});
		MyApp.requestQueue.add(get);
	}

	public void logout(){

		//Intent intent=new Intent(MainActivity.this,LoginActivity.class);
		//startActivity(intent);
		finish();
	}

	@Override
	public void getMessageSuccess(String msg) {
		if (msg.contains("正常")) {
			System.out.println("正常");
			//stateImg.setImageDrawable(getResources().getDrawable((R.drawable.zhengchang)));
		} else if (msg.contains("脱落")) {
			System.out.println("正常");
			//stateImg.setImageDrawable(getResources().getDrawable((R.drawable.tuoluo)));
		} else {
			//stateImg.setImageDrawable(getResources().getDrawable((R.drawable.duan)));
		}
	}

	@Override
	public void onMessageReceived(List<EMMessage> messages) {

		System.out.println("------------------MMM::----------------------");
		String msg =new String();
		for (EMMessage message:messages){
			 msg = ((EMTextMessageBody)message.getBody()).getMessage();
			System.out.println("MMM::"+msg);
		}
		Intent intent = new Intent();
		intent.setAction("polly.liu.Image");//用隐式意图来启动广播
		intent.putExtra("msg", msg);
		MyApp.getContext().sendBroadcast(intent);

	}

	@Override
	public void onCmdMessageReceived(List<EMMessage> messages) {

	}

	@Override
	public void onMessageRead(List<EMMessage> messages) {

	}

	@Override
	public void onMessageDelivered(List<EMMessage> messages) {

	}

	@Override
	public void onMessageChanged(EMMessage message, Object change) {

	}

	@Override
	protected void onResume() {
		super.onResume();
		EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
	}

	@Override
	protected void onStop(){
		super.onStop();
		EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
	}

	@Override
	protected void onPause() {
		super.onPause();
		System.out.println("onResumeonResumeonResumeonResumeonResumeMMM::");
		EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();
		EMClient.getInstance().chatManager().removeMessageListener(mMessageListener);
	}
	/**
	 * 判断程序是否在前台运行
	 * @return true在前台，false在后台
	 */
	public boolean isAppOnForeground() {
		// Returns a list of application processes that are running on the
		// device

		ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
		String packageName = getApplicationContext().getPackageName();

		List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
				.getRunningAppProcesses();
		if (appProcesses == null)
			return false;

		for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
			// The name of the process that this object is associated with.
			if (appProcess.processName.equals(packageName)
					&& appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
				return true;
			}
		}
		return false;
	}

}