package com.example.bluetooth.le.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.LoginActivity;
import com.example.bluetooth.le.Message;
import com.example.bluetooth.le.comm.MacAddress;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SynthesizerListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

public class HistoryFragment extends Fragment  {

    //listview列表
    private ListView listView;
    //数据适配器
    private SimpleAdapter simp_adapter;
    //被展示数据源
    private List<Map<String,Object>> dataList;
    private TextToSpeech mSpeech;
    // 消息监听器
    private EMMessageListener mMessageListener;

    TextToSpeech tts;

    public HistoryFragment() {
        // Required empty public constructor
    }

    BroadcastMain receiver;

    //内部类，实现BroadcastReceiver
     public class BroadcastMain extends BroadcastReceiver {
        //必须要重载的方法，用来监听是否有广播发送
        @Override
        public void onReceive(Context context, Intent intent) {
            final String msg = intent.getStringExtra("msg");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        tts.setPitch(1.0f);
                        // 设置语速
                        tts.setSpeechRate(0.91f);
//                        SpeechSynthesizer mTts=SpeechSynthesizer.createSynthesizer(getActivity(), null);
//                        //2.合成参数设置
//                        mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
//                        mTts.setParameter(SpeechConstant.SPEED, "40");//设置语速
//                        mTts.setParameter(SpeechConstant.VOLUME, "98");//设置音量，范围 0~100
//                        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD); //设置云端
//                        Thread.sleep(200);//休眠0.2秒，模拟耗时操作
                       // if(msg.contains("脱落")||msg.contains("断开")||msg.contains("连接")){
                        tts.speak(msg, TextToSpeech.QUEUE_FLUSH, null);
                        //}
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
            getHistoryData();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, null);
        listView = (ListView) rootView.findViewById(R.id.listView);
        listView.addFooterView(new ViewStub(getActivity()));
        //适配器加载数据源
        dataList = new ArrayList<Map<String,Object>>();
        receiver = new BroadcastMain();
        EMClient.getInstance().chatManager().addMessageListener(mMessageListener);
        tts = new TextToSpeech(getActivity(), null);
        IntentFilter filter = new IntentFilter();
        filter.addAction("polly.liu.Image");
        getActivity().registerReceiver(receiver, filter);
        getHistoryData();
        return rootView;

    }


    @Override
    public void onResume() {
        super.onResume();
        getHistoryData();
    }

    private void getHistoryData(){

        String httpUrl = Url.baseUrl+Url.infors;
        StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?userName="+
                UserName.getInstance().getUserName(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    JSONArray warnInforsList = object.getJSONArray("warnInforsList");
                    dataList.clear();
                    if(warnInforsList.length()>0) {
                        for (int i = 0; i < warnInforsList.length(); i++) {
                            Map<String, Object> map = new HashMap<String, Object>();
                            JSONObject warnInfor =  warnInforsList.getJSONObject(i);
                            if(warnInfor.getString("deviceState").contains("正常")){
                                map.put("img", R.drawable.normal);
                            }else if(warnInfor.getString("deviceState").contains("脱落")){
                                map.put("img", R.drawable.yichangs);
                            }else{
                                map.put("img", R.drawable.duan);
                            }
                            map.put("tvName", warnInfor.getString("childName"));
                            map.put("tvTime", ""+warnInfor.getString("dateTime"));
                            dataList.add(map);
                        }

                        simp_adapter = new SimpleAdapter(getActivity(), dataList, R.layout.listview_item,
                                new String[]{"img","tvName","tvTime"}, new int[]{R.id.ivImage,R.id.tvName,R.id.tvTime});
                        listView.setAdapter(simp_adapter);
                    }else{
                        Toast.makeText(getActivity(), "无记录", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(getActivity(), "网络错误", Toast.LENGTH_SHORT).show();
            }
        });
        MyApp.requestQueue.add(get);
    }
}
