package com.example.bluetooth.le;

/**
 * Created by KWJ on 2017/6/21.
 */
public class Fruit {

    private int imageId;          //使用id锁定水果图片
    private String imageName;     //对应的水果名字
    public Fruit(String imageName,int imageId) {
        super();
        this.imageId = imageId;
        this.imageName = imageName;
    }
    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public String getImageName() {
        return imageName;
    }
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}