package com.example.bluetooth.le;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.PersonAdapter;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;
import com.example.bluetooth.le.comm.ViewHolder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

public class BindHistoryActivity extends TitleActivity implements View.OnClickListener  {

    ListView lv = null;
    Button btn_selectAll = null,btn_sure,btn_cancel;

    ArrayList<JSONObject> listData = null;
    private List<HashMap<String, Object>> list = null;
    private PersonAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("未关联病人");
        showBackwardView(0, true);
        setContentView(R.layout.activity_bind_history);
        mForwardButton.setText("全选");

        lv = (ListView) this.findViewById(R.id.list_view);
        btn_selectAll = (Button) this.findViewById(R.id.selectall);
        btn_selectAll.setOnClickListener(this);
        btn_sure = (Button) this.findViewById(R.id.sure);
        btn_sure.setOnClickListener(this);
        btn_cancel = (Button) this.findViewById(R.id.cancel);
        btn_cancel.setOnClickListener(this);
        getBindHistoryData();
    }

    private void getBindHistoryData(){

        String httpUrl = Url.setUrl(Url.bindHistory);
        StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?userName="+
                UserName.getInstance().getUserName(), new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    JSONArray bindList = object.getJSONArray("bindList");
                    if(bindList.length()>0) {
                       showCheckBoxListView(bindList);
                    }else{
                        Toast.makeText(MyApp.getContext(), "无记录", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MyApp.getContext(), "网络错误", Toast.LENGTH_SHORT).show();
            }
        });
        MyApp.requestQueue.add(get);
    }

    public void showCheckBoxListView(final JSONArray bindList) throws JSONException {

        list = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < bindList.length(); i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("name", bindList.getJSONObject(i).getString("childName"));
            map.put("time", bindList.getJSONObject(i).getString("createTime"));
            map.put("checkBox", false);
            list.add(map);

            adapter = new PersonAdapter(this, list, R.layout.activity_item,
                    new String[] { "name", "time","checkBox" }, new int[] {
                    R.id.name, R.id.tvTime,R.id.checkBox });
            lv.setAdapter(adapter);
            listData = new ArrayList<JSONObject>();
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View view,
                                        int position, long arg3) {
                    ViewHolder holder = (ViewHolder) view.getTag();
                    holder.checkBox.toggle();
                    adapter.isSelected.put(position, holder.checkBox.isChecked());
                    adapter.notifyDataSetChanged();
                    try {
                        JSONObject temp = bindList.getJSONObject(position);
                        if (holder.checkBox.isChecked() == true) {
                            if(!temp.getString("userName").equals(UserName.getInstance().getUserName())){
                                listData.add(temp);
                            }
                        } else {
                            listData.remove(temp);
                        }
                    } catch (JSONException e) {

                    }
                }

            });

        }
    }

    private void bindDevice() {

        String httpUrl = Url.setUrl(Url.bindChild);
        List<String> childList = new ArrayList<String>();
        for (int i=0;i<listData.size();i++){
            try {
                JSONObject temp = listData.get(i);
                String obj = temp.getString("macAddress")+"pp"+temp.getString("childName")+"pp"+UserName.getInstance().getUserName();
                childList.add(obj);
            } catch (JSONException e) {

            }
        }

        final Map<String, String> map = new HashMap<String, String>();
        String str = childList.toString().replace(" ", "");
        map.put("childList", str.substring(1,str.length()-1));
        StringRequest post = new StringRequest(Request.Method.POST, httpUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                Toast.makeText(MyApp.getContext(), "关联成功！", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(MyApp.getContext(), "网络错误", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApp.requestQueue.add(post);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_backward:
                finish();
                break;
            case R.id.sure:
                bindDevice();
                break;
            case R.id.cancel:
                for(int i=0;i<list.size();i++){
                    adapter.isSelected.put(i,false);
                }
                adapter.notifyDataSetChanged();
                break;
            case R.id.selectall:
                for(int i=0;i<list.size();i++){
                    adapter.isSelected.put(i,true);
                }
                adapter.notifyDataSetChanged();
                break;
            default:
                break;
        }

    }

}
