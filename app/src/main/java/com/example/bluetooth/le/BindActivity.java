package com.example.bluetooth.le;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.DBManager;
import com.example.bluetooth.le.comm.MacAddress;
import com.example.bluetooth.le.comm.MacModel;
import com.example.bluetooth.le.comm.Url;
import com.example.bluetooth.le.comm.UserName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;


public class BindActivity extends TitleActivity implements View.OnClickListener {

    private final static int SCANNIN_GREQUEST_CODE = 1;
    private EditText etUserName,etPassword,etPasswordSure;
    private Button btnBind,btnRegister;
    private String macAddress = "",update = "";
    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("关联设备");
        showForwardView(false);
        showBackwardView(0, true);
        setContentView(R.layout.activity_bind);
        dbManager = new DBManager(this);
        etUserName = (EditText) findViewById(R.id.et_username);
        btnBind = (Button)findViewById(R.id.btn_bind);
        btnBind.setOnClickListener(this);
        btnRegister = (Button)findViewById(R.id.btn_bind_sure);
        btnRegister.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        switch (view.getId()) {

            case R.id.btn_bind:
                Intent intent = new Intent();
                intent.setClass(BindActivity.this,CaptureActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
                break;
            case R.id.btn_bind_sure:
                if(check()){
                    bindDevice();
                }
                break;
            case R.id.button_backward:
                finish();
                break;
            default:
                break;
        }
    }

    private boolean check(){

        if(etUserName.getText().toString().trim().equals("")){
            Toast.makeText(BindActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

    private void bindDevice(){

        String httpUrl = Url.baseUrl+Url.bind;
        final Map<String, String> map = new HashMap<String, String>();
        map.put("userName", UserName.getInstance().getUserName());
        map.put("childName", etUserName.getText().toString());
        map.put("macAddress",macAddress);
        map.put("update",update);
        System.out.println( "volleyPos请求成功：" + map);
        StringRequest post = new StringRequest(Request.Method.POST, httpUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                System.out.println( "volleyPostStringMonth请求成功：" + s);
                Toast.makeText(BindActivity.this, "设备绑定成功！", Toast.LENGTH_SHORT).show();
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(BindActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //创建一个集合，放的是keyvalue的key是参数名与value是参数值
                return map;
            }
        };
        MyApp.requestQueue.add(post);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SCANNIN_GREQUEST_CODE:
                if(resultCode == RESULT_OK){
                    Bundle bundle = data.getExtras();
                    //显示扫描到的内容
                    macAddress = bundle.getString("result");
                    Toast.makeText(BindActivity.this, "设备已绑定", Toast.LENGTH_SHORT).show();
                    System.out.println("macAddress:"+macAddress);
                    getChildName(macAddress);

                }
                break;
        }
    }

    private void getChildName(String macAddress){

        String httpUrl = Url.baseUrl+Url.childName;
        StringRequest get = new StringRequest(Request.Method.GET, httpUrl+"?macAddress="+
                macAddress, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    final String childName = object.getString("childName");
                    if(childName != null){
                        AlertDialog.Builder builder = new AlertDialog.Builder(
                                BindActivity.this,AlertDialog.THEME_HOLO_LIGHT);
                        builder.setTitle("提示");
                        builder.setMessage("该设备已和"+childName+"用户相关联，是否更改用户或床位？");
                        builder.setPositiveButton("是",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        update = "update";
                                    }
                                });
                        builder.setNegativeButton("否",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        etUserName.setText(childName);
                                    }
                                });
                        builder.show();
                    }else{

                    }
                } catch (JSONException e) {

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Toast.makeText(BindActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
            }
        });
        MyApp.requestQueue.add(get);
    }

}

