package com.example.bluetooth.le;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.bluetooth.le.comm.MacAddress;
import com.example.bluetooth.le.comm.Url;
import com.hyphenate.chat.EMClient;
import com.hyphenate.exceptions.HyphenateException;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cfm.com.cfmclient.MyApp;
import cfm.com.cfmclient.R;

public class RegisterActivity extends TitleActivity implements View.OnClickListener {

	private final static int SCANNIN_GREQUEST_CODE = 1;
	private EditText etUserName,etPassword,etPasswordSure;
	private Button btnBind,btnRegister;
    private String macAddress = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("注册");
		showForwardView(false);
		showBackwardView(0, true);
		setContentView(R.layout.activity_register);
		etUserName = (EditText) findViewById(R.id.et_username);
		etPassword = (EditText) findViewById(R.id.et_password);
		etPasswordSure = (EditText) findViewById(R.id.et_password_sure);
	//	btnBind = (Button)findViewById(R.id.btn_bind);
		//btnBind.setOnClickListener(this);
		btnRegister = (Button)findViewById(R.id.btn_register);
		btnRegister.setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {

//			case R.id.btn_bind:
//				Intent intent = new Intent();
//				intent.setClass(RegisterActivity.this,CaptureActivity.class);
//				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//				startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
//				break;
			case R.id.btn_register:
				if(check()){
					register();
				}
				break;
			case R.id.button_backward:
				finish();
				break;
			default:
				break;
		}
	}

	private boolean check(){

		if(etUserName.getText().toString().trim().equals("")){
			Toast.makeText(RegisterActivity.this, "用户名不能为空", Toast.LENGTH_SHORT).show();
			return false;
		}else if(etPassword.getText().toString().trim().equals("")){
			Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
			return false;
		}else{
			return true;
		}
	}

	private void register(){
new Thread(new Runnable() {
	@Override
	public void run() {
		try {
			EMClient.getInstance().createAccount(etUserName.getText().toString().trim(),etUserName.getText().toString().trim().charAt(0)+"1");
		} catch (HyphenateException e) {
			Toast.makeText(RegisterActivity.this, "环信注册失败！"+etUserName.getText().toString().trim().charAt(0)+"1", Toast.LENGTH_SHORT).show();
		}
	}
}).start();
		String httpUrl = Url.baseUrl + Url.register;
		final Map<String, String> map = new HashMap<String, String>();
		map.put("userName", etUserName.getText().toString().trim());
		map.put("passWord", etPassword.getText().toString().trim());
		map.put("macAddress","m");

		StringRequest post = new StringRequest(Request.Method.POST, httpUrl, new Response.Listener<String>() {
			@Override
			public void onResponse(String s) {

				try {
					JSONObject object = new JSONObject(s);
					String registerState = object.getString("registerState");
					if(registerState.equals("success")){
						MacAddress.setInstance(new MacAddress(macAddress));
						Toast.makeText(RegisterActivity.this, "注册成功！", Toast.LENGTH_SHORT).show();
						Intent intent2 = new Intent(RegisterActivity.this,LoginActivity.class);
						startActivity(intent2);
					}else{
						Toast.makeText(RegisterActivity.this,registerState, Toast.LENGTH_SHORT).show();
					}
				} catch (JSONException e) {

				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError volleyError) {
				Toast.makeText(RegisterActivity.this, "网络错误", Toast.LENGTH_SHORT).show();
			}
		}) {
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				//创建一个集合，放的是keyvalue的key是参数名与value是参数值
				return map;
			}
		};
		MyApp.requestQueue.add(post);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case SCANNIN_GREQUEST_CODE:
				if(resultCode == RESULT_OK){
					Bundle bundle = data.getExtras();
					//显示扫描到的内容
					macAddress = bundle.getString("result");
					System.out.println("macAddress:"+macAddress);
				//	MyApp.dbManager.add(macAddress);
				}
				break;
		}
	}
}
