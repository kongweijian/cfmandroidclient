package com.example.bluetooth.le.comm;

public class UserName {

    private String userName;

	private volatile static UserName instance;

	public UserName(String userName){
		this.userName=userName;
	}

	public UserName(){

	}

	public static UserName getInstance() {
		if (instance == null) {
			instance = new UserName();
		}
		return instance;
	}

	public static void setInstance(UserName instance) {
		UserName.instance = instance;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
