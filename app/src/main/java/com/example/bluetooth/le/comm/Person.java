package com.example.bluetooth.le.comm;

/**
 * Created by whjth on 2017/6/16.
 */

public class Person {
    private int personId;
    private String name;
    private int ImageId;

    public Person(int personId,String name, int ImageId){
        this.personId=personId;
        this.name = name;
        this.ImageId = ImageId;
    }
    public int getPersonId(){
        return personId;
    }
    public String getName(){
        return name;
    }
    public int getImageId(){
        return ImageId;
    }
}
