package com.example.bluetooth.le.comm;

import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by whjth on 2017/6/16.
 */

public class ViewHolder {
    ImageView image;
    TextView name;
    TextView time;
    View delete;
    View bind;
    public CheckBox checkBox;
}
