package cfm.com.cfmclient;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.bluetooth.le.comm.DBManager;
import com.example.bluetooth.le.comm.MacModel;

import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMOptions;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.mob.MobSDK;

import cn.smssdk.SMSSDK;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


/**
 * Created by Allen Lin on 2016/02/17.
 */
public class MyApp extends Application {

    private static Context sContext;
    public static RequestQueue requestQueue;
    private DBManager dbManager;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        requestQueue = Volley.newRequestQueue(sContext);
        dbManager = new DBManager(sContext);
        // 启动两个守护服务
        MobSDK.init(sContext, "228220af8094d", "c3315634e2d5d954fbb9212833f52ccb");
        SpeechUtility.createUtility(sContext, SpeechConstant.APPID +"=5a113ac4");
//        JPushInterface.setDebugMode(true);//正式版的时候设置false，关闭调试
      //  appContext = this;
        int pid = android.os.Process.myPid();
        String processAppName = getAppName(pid);
// 如果APP启用了远程的service，此application:onCreate会被调用2次
// 为了防止环信SDK被初始化2次，加此判断会保证SDK被初始化1次
// 默认的APP会在以包名为默认的process name下运行，如果查到的process name不是APP的process name就立即返回

        if (processAppName == null ||!processAppName.equalsIgnoreCase(sContext.getPackageName())) {
            Log.e("ww", "enter the service process!");

            // 则此application::onCreate 是被service 调用的，直接返回
            return;
        }
        EMOptions options = new EMOptions();
        options.setAcceptInvitationAlways(false);
        EMClient.getInstance().init(sContext, options);
        EMClient.getInstance().setDebugMode(true);

    }

    public static Context getContext() {
        return sContext;
    }


    private String getAppName(int pID) {
        String processName = null;
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List l = am.getRunningAppProcesses();
        Iterator i = l.iterator();
        PackageManager pm = this.getPackageManager();
        while (i.hasNext()) {
            ActivityManager.RunningAppProcessInfo info = (ActivityManager.RunningAppProcessInfo) (i.next());
            try {
                if (info.pid == pID) {
                    processName = info.processName;
                    return processName;
                }
            } catch (Exception e) {
                // Log.d("Process", "Error>> :"+ e.toString());
            }
        }
        return processName;
    }

}

